from src.methods.fatima_ezzahra_ouahmane.component_substitution import component_substitution
from src.methods.fatima_ezzahra_ouahmane.generalized_scheme import cs_generalized_scheme

import numpy as np


import numpy as np

def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray) -> list:
    reconstructed_ms1_cs = component_substitution(pan, ms1)
    reconstructed_ms2_cs = component_substitution(pan, ms2)

    reconstructed_ms1_generalized = cs_generalized_scheme(pan, ms1)
    reconstructed_ms2_generalized = cs_generalized_scheme(pan, ms2)

    # Redimensionner reconstructed_ms2_cs pour qu'il ait la même forme que reconstructed_ms1_cs
    reconstructed_ms2_cs_resized = resize(reconstructed_ms2_cs, reconstructed_ms1_cs.shape[:2], anti_aliasing=True)
    combined_result_cs = (reconstructed_ms1_cs + reconstructed_ms2_cs_resized) / 2
    
    reconstructed_ms2_generalized_resized = resize(reconstructed_ms2_generalized, reconstructed_ms2_generalized.shape[:2],      anti_aliasing=True)
    combined_result_generalized = (reconstructed_ms1_generalized + reconstructed_ms2_generalized_resized )  / 2

    # Retourner une liste d'images
    return [combined_result_cs,combined_result_generalized]
