"""A file containing some useful checks for the project.
This file should NOT be modified.
"""


from os.path import exists
import numpy as np



def check_path(file_path: str) -> None:
    """Checks if a file exists at file_path and is a png image.

    Args:
        file_path (str): Path to check

    Raises:
        Exception: Exception if the path is invalid.
    """
    if not exists(file_path):
        raise Exception(f'File {file_path} does not exist.')


def check_png(file_path: str) -> None:
    """Checks if the file is a png image.

    Args:
        file_path (str): Path to check.
    """
    if not file_path.endswith('.png'):
        raise Exception(f'Path must end with ".png". Got {file_path[-4:]}.')


def check_csv(file_path: str) -> None:
    """Checks if the file is a csv file.

    Args:
        file_path (str): Path to check.
    """
    if not file_path.endswith('.csv'):
        raise Exception(f'Path must end with ".csv". Got {file_path[-4:]}.')


def check_npy(file_path: str) -> None:
    """Checks if the file is a npy file.

    Args:
        file_path (str): Path to check.
    """
    if not file_path.endswith('.npy'):
        raise Exception(f'Path must end with ".npy". Got {file_path[-4:]}.')


def check_shape(img1: np.ndarray, img2: np.ndarray) -> None:
    """Checks if img1 and img2 have the same shape.

    Args:
        img1 (np.ndarray): First image.
        img2 (np.ndarray): Second image.

    Raises:
        Exception: Exception if img1 and img2 do not have the same shape.
    """
    if img1.shape != img2.shape:
        raise Exception(f'The images must have the same shape. Got {img1.shape} and {img2.shape}.')


def check_data_range(img: np.ndarray) -> None:
    """Checks if the values of img are in the interval [0, 1].

    Args:
        img (np.ndarray): Image to check.

    Raises:
        Exception: Exception if img's values are not in [0, 1].
    """
    if np.max(img) > 1 or np.min(img) < 0:
        raise Exception(f'Pixel\'s values must be in range [0, 1]. Got range [{np.min(img)}, {np.max(img)}].')


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
